﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SqlServerUtilities
{
    public class ResultItem
    {
        public string DBName { get; set; }
        public bool Successful { get; set; }
        public string Result { get; set; }
    }
}
