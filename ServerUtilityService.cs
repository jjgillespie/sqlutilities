﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace SqlServerUtilities
{
    public class ServerUtilityService
    {
        public List<string> GetDatabaseList(string connString)
        {
            var dbList = new List<string>();
            string sql = "select distinct name from master..sysdatabases " +
                            " where name not in ('master', 'tempdb', 'msdb', 'model') " +
                            " order by name ";
            using (var conn = new SqlConnection(connString))
            {
                conn.Open();
                var cmd = new SqlCommand(sql, conn);
                var dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    dbList.Add(dr.GetString(0));
                }
            }
            return dbList;
        }
    }
}
