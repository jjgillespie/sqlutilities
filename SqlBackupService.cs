﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;

namespace SqlServerUtilities
{
    public class SqlBackupService
    {
        public List<ResultItem> Backup(List<string> dbNameList, string connString, string backupLocation, ServerMessageEventHandler onCompleted = null, PercentCompleteEventHandler onPercentComplete = null)
        {
            return dbNameList.Select(db => Backup(db, connString, backupLocation, onCompleted, onPercentComplete)).ToList();
        }

        public ResultItem Backup(string dbName, string connString, string backupLocation, ServerMessageEventHandler onCompleted = null, PercentCompleteEventHandler onPercentComplete = null)
        {
            var backup = InitializeBackup(dbName, backupLocation, onCompleted, onPercentComplete);
            return BackupDatabase(backup, connString);
        }

        private ResultItem BackupDatabase(Backup backup, string connString)
        {

            using (var conn = new SqlConnection(connString))
            {
                conn.Open();

                var server = new Server(new ServerConnection(conn));
                
                try
                {
                    backup.SqlBackup(server);
                    return new ResultItem()
                               {
                                   DBName = backup.Database,
                                   Successful = true,
                                   Result = "Completed"
                               };
                }
                catch (Exception ex)
                {
                    return new ResultItem()
                               {
                                   DBName = backup.Database,
                                   Successful = false,
                                   Result = "Error: " + ex.Message + " -- " + ex.InnerException.Message
                               };
                }
            }
        }

        private Backup InitializeBackup(string dbName, string backupLocation, ServerMessageEventHandler onCompleted = null, PercentCompleteEventHandler onPercentComplete = null)
        {
            var backup = new Backup
                             {
                                 Action = BackupActionType.Database,
                                 Database = dbName,
                                 BackupSetName = dbName + " Backup (SQLExec)",
                                 BackupSetDescription = "Backup for " + dbName + " Created by the SQLExec tool",
                                 Initialize = false
                             };

            backup.Devices.AddDevice(BuildBakName(dbName, backupLocation), DeviceType.File);
            backup.Complete += onCompleted;
            backup.PercentComplete += onPercentComplete;

            return backup;
        }

        private string BuildBakName(string dbName, string backupLocation)
        {
            return string.Format("{0}\\{1}_{2}.bak", backupLocation, dbName, DateTime.Now.ToString("yyMMdd_HHmmss"));
        }
    }
}
