﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;

namespace SqlServerUtilities
{
    public class SqlRestoreService
    {
        //public List<ResultItem> Restore(List<string> dbNameList, string connString, string backupLocation, ServerMessageEventHandler onCompleted = null, PercentCompleteEventHandler onPercentComplete = null)
        //{
        //    return dbNameList.Select(db => Backup(db, connString, backupLocation, onCompleted, onPercentComplete)).ToList();
        //}

        public ResultItem Restore(string dbName, string originalDbName, string connString, string backupLocation, bool replaceExistingDb = false, ServerMessageEventHandler onCompleted = null, PercentCompleteEventHandler onPercentComplete = null)
        {
            var restore = InitializeRestore(dbName, backupLocation, replaceExistingDb, onCompleted, onPercentComplete);
            return RestoreDatabase(restore, originalDbName, connString);
        }

        private ResultItem RestoreDatabase(Restore restore, string originalDbName, string connString)
        {
            using (var conn = new SqlConnection(connString))
            {
                conn.Open();
                var server = new Server(new ServerConnection(conn));
                try
                {

                    if(!restore.ReplaceDatabase)
                    {
                        DataTable dtFileList = restore.ReadFileList(server);
                        string dbLogicalName = dtFileList.Rows[0][0].ToString();
                        string dbPhysicalName = dtFileList.Rows[0][1].ToString();
                        string logLogicalName = dtFileList.Rows[1][0].ToString();
                        string logPhysicalName = dtFileList.Rows[1][1].ToString();

                        //add mdf file
                        var existingDataFilePath = server.Databases[1].FileGroups[0].Files[0].FileName;
                        var existingDataFileName = existingDataFilePath.Split('\\').Last();
                        var newDataFilePath = existingDataFilePath.Replace(existingDataFileName, restore.Database + "_Data.mdf");
                        restore.RelocateFiles.Add(new RelocateFile(dbLogicalName, newDataFilePath));
                        //add log file
                        var existingLogFilePath = server.Databases[0].LogFiles[0].FileName;
                        var existingLogFileName = existingDataFilePath.Split('\\').Last();
                        var newLogFilePath = existingDataFilePath.Replace(existingDataFileName, restore.Database + "_Log.ldf");
                        restore.RelocateFiles.Add(new RelocateFile(logLogicalName, newLogFilePath));
                        restore.ReplaceDatabase = true;
                    }


                    restore.SqlRestore(server);
                    return new ResultItem()
                               {
                                   DBName = restore.Database,
                                   Successful = true,
                                   Result = "Completed"
                               };
                }
                catch (Exception ex)
                {
                    return new ResultItem()
                               {
                                   DBName = restore.Database,
                                   Successful = false,
                                   Result = "Error: " + ex.Message + " -- " + ex.InnerException.Message
                               };
                }
            }
        }

        private Restore InitializeRestore(string dbName, string restoreFileLocation, bool replaceExistingDb, ServerMessageEventHandler onCompleted = null, PercentCompleteEventHandler onPercentComplete = null)
        {
            var restore = new Restore
                             {
                                 Action = RestoreActionType.Database,
                                 ReplaceDatabase = replaceExistingDb,
                                 Database = dbName
                             };
            
            restore.Devices.AddDevice(restoreFileLocation, DeviceType.File);
            restore.Complete += onCompleted;
            restore.PercentComplete += onPercentComplete;

            return restore;
        }

        private string BuildBakName(string dbName, string backupLocation)
        {
            return string.Format("{0}\\{1}_{2}.bak", backupLocation, dbName, DateTime.Now.ToString("yyMMdd_HHmmss"));
        }
    }
}
