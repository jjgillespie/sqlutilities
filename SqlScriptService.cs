﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Common;

namespace SqlServerUtilities
{
    public class SqlScriptService
    {
        public ResultItem ExecuteSql(string dbName, string connString, string sql, bool wrapInTransaction)
        {
            using (var conn = new SqlConnection(connString))
            {
                conn.Open();

                var server = new Server(new ServerConnection(conn));
                if (wrapInTransaction)
                {
                    server.ConnectionContext.BeginTransaction();
                }

                try
                {
                    var rowsAffected = server.ConnectionContext.ExecuteNonQuery(sql);
                    if (wrapInTransaction)
                    {
                        server.ConnectionContext.CommitTransaction();
                    }
                    return new ResultItem()
                    {
                        DBName = dbName,
                        Successful = true,
                        Result = rowsAffected + " rows affected."
                    };
                }
                catch (Exception ex)
                {
                    try
                    {
                        var msg = string.Empty;
                        if (wrapInTransaction)
                        {
                            server.ConnectionContext.RollBackTransaction();
                            msg = "Trans rolled back. ";
                        }

                        msg += ex.Message;
                        if (ex.InnerException != null)
                            msg += " ----- Inner Exception: " + ex.InnerException.Message;
                        return new ResultItem()
                        {
                            DBName = dbName,
                            Successful = false,
                            Result = msg
                        };
                    }
                    catch (Exception ex2)
                    {
                        //string msg = "Error rolling back transaction for db (" + connString + "): " + ex2.Message;
                        //if (ex2.InnerException != null)
                        //    msg += "  Inner Exception: " + ex2.InnerException.Message;
                        //MessageBox.Show(msg);
                        throw;
                    }
                }
            }
            return new ResultItem()
            {
                DBName = dbName,
                Successful = false,
                Result = "The execution failed"
            };
        }
    }
}
